const mongoose = require("mongoose");

const Location = mongoose.model(
    "Location",
    new mongoose.Schema({
        latitude: String,
        longitude : String,
    })
  );

  module.exports = Location;