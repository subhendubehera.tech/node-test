const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;

checkDuplicateMobile = (req, res, next) => {
  // Username
  User.findOne({
    mobile: req.body.mobile
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(400).send({ message: "Failed! mobile is already in use!" });
      return;
    }
    next();
  });
};

const verifySignUp = {
  checkDuplicateMobile,
};

module.exports = verifySignUp;
