const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Location = db.location;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  // return res.status(200).send(req.body.location);

  const location = new Location({
    latitude : req.body.location.latitude,
    longitude: req.body.location.longitude,
  })
  location.save();
  // return res.status(200).send(location);
  // location.save((err, location)=>{
  //   res.status(200).send(location);
  // })

  const user = new User({
    name: req.body.name,
    mobile: req.body.mobile,
    password: bcrypt.hashSync(req.body.password, 8),
    location: location._id
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map((role) => role._id);
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "Registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ message: "Registered successfully!" });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    mobile: req.body.mobile,
  })
    .populate("roles", "-__v")
    .populate("location","-__v",)
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({ message: "Invalid Password!" });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });

      
      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push(user.roles[i].name.toUpperCase());
      }
      res.status(200).send({
        id: user._id,
        name: user.name,
        roles: authorities,
        token:token,
        mobile:user.mobile,
        location:user.location
      });
    });
};

