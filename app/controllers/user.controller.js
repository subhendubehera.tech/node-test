const db = require("../models");
const User = db.user;
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.findAllUsers = (req, res) => {
  User.find()
  .populate("roles", "-__v")
  .populate("location", "-__v")
  .then(result=>{
    res.status(200).json(result);
  })
  .catch(err=>{
    res.status(500).json({
      error:err
    })
  })
};

exports.userdetails_jwt = (req, res) => {
  const token = req.headers.authorization.replace("Bearer ", "");
  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }
  const userDetail = jwt.verify(token, config.secret);
  User.findById(userDetail.id)
  .then(result=>{
    res.status(200).json({result});
  })
  .catch(err=>{
    res.json({ message: "user not found" });
  })
};
